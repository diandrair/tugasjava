package tugasJava1;

import java.util.Arrays;
import java.util.Scanner;

public class NumberToStack {
	
	
	
    public static void main (String[] args) {
    
    	int arr[] = { 1, 83, 67, 4, 5, 74, 7, 8, 99, 10 };
    	System.out.println("Initial Array:\n" + Arrays.toString(arr));
    	Scanner input = new Scanner(System.in);

    	
		System.out.println("Input number to stack :");
		int x = input.nextInt();
		
		
    	int n = 10;
    	
   
    	StackImpl stack = new StackImpl();
    	
    	int[] push = stack.push(n, arr, x);
    	
    	
        // print the original array
        System.out.println("Array after push operation:\n"
                           + Arrays.toString(push));
        
        int[] popped = stack.pop(push);
        System.out.println("Array after pop operation:\n"
                + Arrays.toString(popped));
   
        int max = stack.max(popped);
        System.out.println("Array maximum value :\n"
                + max);
        
        int size = stack.size(popped);
        System.out.println("Array size :\n"
                + size);
    }




}
