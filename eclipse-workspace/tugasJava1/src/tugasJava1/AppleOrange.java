package tugasJava1;

import java.util.Scanner;

public class AppleOrange {
	public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("input titik awal rumah:");
        int s = in.nextInt();
        System.out.println("input titik akhir rumah:");
        int t = in.nextInt();
        System.out.println("input titik pohon apple:");
        int a = in.nextInt();
        System.out.println("input titik pohon orange:");
        int b = in.nextInt();
        System.out.println("input banyaknya apple:");
        int m = in.nextInt();
        System.out.println("input banyaknya orange:");
        int n = in.nextInt();
        int[] apple = new int[m];
        int app = 0;
        for(int apple_i=0; apple_i < m; apple_i++){
        	System.out.println("input titik jatuh apple"+apple_i +":");
            apple[apple_i] = in.nextInt();
            if (a + apple[apple_i] >= s && a + apple[apple_i] <= t) {
                app++;
            }
        }
        int[] orange = new int[n];
        int or = 0;
        for(int orange_i=0; orange_i < n; orange_i++){
        	System.out.println("input titik jatuh orange"+orange_i +":");
            orange[orange_i] = in.nextInt();
            if (b + orange[orange_i] >= s && b + orange[orange_i] <= t) {
                or++;
            }
        }
        
        System.out.println("total apple:"+ app);
        System.out.println("total orange:"+ or);
    }

}
